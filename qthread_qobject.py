#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 12:38:34 2016

@author: redbaron
"""

import sys, time
from PyQt4 import QtGui, QtCore

class Worker(QtCore.QObject):

  def __init__(self, parent=None):
    QtCore.QObject.__init__(self)

  @QtCore.pyqtSlot()
  def do(self):
    print('Worker thread', QtCore.QThread.currentThread())

    for i in range(0, 10):
      print('Do', i)
      time.sleep(1)

class Example(QtGui.QWidget):

  def __init__(self):
    super(Example, self).__init__()
    self.signal = QtCore.SIGNAL('work')
    self.initUI()
    self.initLogic()

  def initUI(self):
    self.button = QtGui.QPushButton('Do work', self)
    self.button.resize(self.button.sizeHint())
    self.button.move(50, 50)

    self.setGeometry(300, 300, 250, 150)
    self.setWindowTitle('QObject and threads')
    self.show()

  def initLogic(self):
    self.thread = QtCore.QThread()
    worker = Worker()
    QtCore.QObject.connect(self.button, QtCore.SIGNAL('clicked()'), worker.do)
    self.thread.start()
    worker.moveToThread(self.thread)

def main():
  app = QtGui.QApplication(sys.argv)
  ex = Example()
  sys.exit(app.exec_())


if __name__ == '__main__':
    main()