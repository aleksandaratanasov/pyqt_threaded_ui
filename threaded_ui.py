#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 22:24:33 2016

@author: redbaron
"""

import sys, time

from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIntValidator
from PyQt5.QtCore import *

# TODO EnableWidget should also make ThreadedWidget pause its Worker thread whenever state is set to disabled
# http://stackoverflow.com/questions/9075837/pause-and-resume-a-qthread


class StatusBarWidget(QStatusBar):
  '''
  A custom QStatusBar that displays statistics on:
    - each TWidget (name and current percentage of its work that has been completed) - this information is displayed as a status message
    - total work amount (from all TWidgets together) and how much has been completed

  Each newly added TWidget is registered in an internal registry (a dictionary) containint the following:
    - [KEY] parent name - each TWidget's thread emits the name of its owner. This name has to be unique
    - [VALUE] (work done, total work amount) - work done (frp, total work amount of the given TWidget) is the value that is also displayed in the progress bar of owning TWidget

  Each TWidget's thread emits a triplet with this data to StatusBarWidget which is received by the self.working_slot
  self.working_slot updates the registry (if a new TWidget is detected) and recalculates the total progress plus updates both the status message and the progress bar
  '''
  def __init__(self, parent=None):
    QStatusBar.__init__(self)
    self.progressbar = QProgressBar()
    self.tws = {}

    self.initUi()

  def initUi(self):
    self.progressbar = QProgressBar()
    self.addPermanentWidget(self.progressbar)
    self.progressbar.setAlignment(Qt.AlignCenter)
    self.showMessage('Status of widgets')
    self.progressbar.setStyleSheet('QProgressBar:horizontal {border: 1px solid gray; border-radius: 10px; background: white; padding: 1px;} QProgressBar::chunk:horizontal {background: qlineargradient(x1: 0, y1: 0.5, x2: 1, y2: 0.5, stop: 0 orange, stop: 1 yellow); border-radius: 10px;}')

  def working_slot(self, val, work_amount, parentName):
    '''
    Slot callback for Worker.signalProgress signal
    Updates the progress bar and the status message with information about the overall work that has been done and the status of each TWidget
    '''
    # The progress signal of the Worker thread also contains the name of its TWidget parent
    # We use this name to fill a dictionary that contains the name as KEY and the current progress + the max work as VALUE

    if parentName not in self.tws:
      self.tws.update({parentName : [val, work_amount]})
    else:
      self.tws[parentName][0] = val         # Update progress
      self.tws[parentName][1] = work_amount # Update work_amount (in case input fieled was enabled again and new value was entered)

    msg = ''
    work_amount_tot = 0
    work_amount_curr = 0
    for tw_name_key, tw_values in self.tws.iteritems():
        work_amount_curr += tw_values[0]  # Sum up all Twidget's current progress value
        work_amount_tot += tw_values[1]   # Sum up all TWidgets' work_amount
    percentage_all = ((float(work_amount_curr) / float(work_amount_tot))*100.) # Calculate percentage based on total amount of work and sum of current progress values for all Twidgets


    for tw_name_key, tw_values in self.tws.iteritems():
      if not tw_values[0] or not tw_values[1]: continue
      # Calculate percentage each TWidget in the dictionary
      percentage = ((float(tw_values[0]) / float(tw_values[1]))*100.)
      msg += 'TW[' + tw_name_key + ' : ' + ('%.2f%%] | ') % percentage
    msg = msg[:-2]
    self.showMessage(msg)


    self.progressbar.setMaximum(work_amount_tot)
    self.progressbar.setValue(work_amount_curr)
    self.progressbar.setFormat(str(work_amount_curr) + ' out of ' + str(work_amount_tot) + (' (%.2f%%)') % percentage_all)


class EnablerWidget(QWidget):
  '''
  Enables/disables all other widgets via a signal
  '''
  signalEnable = pyqtSignal(bool, name='enable')
  def __init__(self, parent=None):
    QWidget.__init__(self)
    self.init_proc = QProcess()

    self.initUi()

  def initUi(self):
    self.layout = QHBoxLayout()

    self.enable_chbox = QCheckBox()
    # The checkbox will be used to trigger an external process that will "initialize" something
    # If the external process finishes without any errors the UI will be enabled and the checkbox disabled
    self.enable_chbox.stateChanged.connect(self.changeUiEnableStatus)
    self.enable_chbox.setText('Init system')

    self.layout.addWidget(self.enable_chbox)
    self.setLayout(self.layout)
    self.show()

  def changeUiEnableStatus(self):
    self.init_proc.start('ls /home/Documents')  # Example command that will be our "initialization"; if the process fails to start or finish properly the UI will remain disabled
    print('Initializing...')
    if not self.init_proc.waitForStarted():
      print('Failed to initialize!')
      self.enable_chbox.setChecked(False)
      return

    if self.init_proc.waitForFinished() and not self.init_proc.exitCode():
      print('Failed to initialize!')
      self.enable_chbox.setChecked(False)
      return

    print('Initialization successful!')
    print(('Enabling' if self.enable_chbox.checkState() else 'Disabling') + ' UI')

    self.signalEnable.emit(self.enable_chbox.checkState())
    
    # Init is complete so we don't need the checkbox to be interactive any longer
    self.enable_chbox.setEnabled(False)

class TheardedWidget(QWidget):
  '''
  A widget that does some work in a separately launched thread, which updates its progress bar
  '''
  def __init__(self, name, parent=None):
    QWidget.__init__(self)
    self.name = name
    # Connect the widget to both signal (progress and finished) that are emitted by a worker thread
    self.thread = Worker()
    self.thread.parentName = name
    self.thread.signalProgress.connect(self.working_slot)
    self.thread.signalFinished.connect(self.finished_slot)

    self.initUi()
    self.setEnabled(False)

  def initUi(self):
    '''
    Initializes all UI-related components
    '''
    self.layout = QVBoxLayout()

    # Use a button to start the worker thread
    self.button = QPushButton('Do work')
    self.button.clicked.connect(self.do_work)
    self.layout.addWidget(self.button)

    self.add_input_widget()

    # Use a progressbar to display the amount of work that has been done so far
    self.progressbar = QProgressBar()
    self.progressbar.setAlignment(Qt.AlignCenter)
    self.progressbar.setStyleSheet('QProgressBar:horizontal {border: 1px solid gray; border-radius: 10px; background: white; padding: 1px;} QProgressBar::chunk:horizontal {background: qlineargradient(x1: 0, y1: 0.5, x2: 1, y2: 0.5, stop: 0 green, stop: 1 lightgreen); border-radius: 10px;}')
    self.progressbar.setMinimum(0)
    self.progressbar.setMaximum(1)
    self.layout.addWidget(self.progressbar)

    self.setLayout(self.layout)
    self.show()

  def add_input_widget(self):
    '''
    Adds a small widget with lined edit and a label
    The line edit is used by the user to specify the amount of work (an integer between 0 and 1000) that the worker thread has to do
    '''
    input_widget = QWidget()
    input_field_layout = QHBoxLayout()
    input_widget.setLayout(input_field_layout)

    label = QLabel('Work (1..n, n<=1000):')
    input_field_layout.addWidget(label)

    self.input = QLineEdit()
    self.input.setValidator(QIntValidator(1, 1000, self))
    self.input.setText('1')

    input_field_layout.addWidget(self.input)
    self.layout.addWidget(input_widget)
    input_widget.show()

  def do_work(self):
    '''
    Triggers the worker thread
    Disables the push button and line edit to prevent the user from resetting the thread while its working
    '''
    work_amount = int(self.input.text())
    self.thread.work_amount = work_amount
    self.progressbar.setMaximum(work_amount)
    self.thread.start()
    self.button.setText('Working...')
    self.button.setDisabled(True)
    self.input.setDisabled(True)

  def working_slot(self, val, work_amount, parentName):
    '''
    Slot callback for Worker.signalProgress signal
    Updates the progress bar
    '''
    self.progressbar.setValue(val)
    percentage = ((float(self.progressbar.value()) / (float(self.progressbar.maximum())))*100.)
    self.progressbar.setFormat(str(val) + ' out of ' + self.input.text() + (' (%.2f%%)') % percentage)

  def finished_slot(self):
    '''
    Slot callback for Worker.signalFinished signal
    Re-enables all previously disabled UI components
    '''
    self.button.setText('Do work')
    self.button.setDisabled(False)
    self.input.setDisabled(False)

  def enable_slot(self, eflag):
    self.setEnabled(eflag)

# TODO Use QObject for the worker and moveToThread to move it to a standard QThread. It's how it's supposed to be done
class Worker(QThread):
  '''
  A worker thread used by each ThreadedWidget to do some specified amount of work
  '''
  signalProgress = pyqtSignal(int, int, str, name='progress')
  signalFinished = pyqtSignal(name='finished')
  def __init__(self):
    QThread.__init__(self)
    self.work_amount = 1
    self.parentName = None

  def run(self):
    '''
    Thread's running method which does the specified amount of work
    It controls when the two thread-signals are emitted:
      - signalProgress - reports to the progress bar of the parent ThreadedWidget how much work has been done so far
      - signalFinished - reports when the thread has finished working (can be replaced by a simple check if signalProgress == work_amount but this demonstrates working with multiple signals)
    '''
    if not self.work_amount:
      print('No work to be done')
      pass

    for i in range(1, self.work_amount+1):
      time.sleep(0.1)
      self.signalProgress.emit(i, self.work_amount, self.parentName)

    self.signalFinished.emit()

class ThreadedWidgetCollection(QWidget):
  '''
  Contains multiple instances of ThreadedWidget
  '''
  def __init__(self, parent=None):
    QWidget.__init__(self)
    self.initUi()

  def initUi(self):
    '''
    Initializes all UI-related components
    '''
    self.setWindowTitle('Threads and Signal Showcase')
    self.layout = QVBoxLayout()
    ew = EnablerWidget()
    self.layout.addWidget(ew)
    sbw = StatusBarWidget()
    for i in range(0, 3):
      tw = TheardedWidget('tw' + str(i))
      ew.signalEnable.connect(tw.enable_slot)
      tw.thread.signalProgress.connect(sbw.working_slot)
      self.layout.addWidget(tw)

    self.layout.addWidget(sbw)
    self.setLayout(self.layout)
    self.show()

'''
# TODO Add a widget to ThreadedWidgetCollection containing a progress bar that displays the TOTAL
    amount of work that is reported by ALL ThreadedWidget instances that are part of it
    In order to do that:
      - get all work_amount values for all ThreadedWidgets
      - capture the each signalProgress (modify the signal to also include the thread ID that emits it so that we can distinguish which progress is part of which ThreadedWidget)

    This will demonstrate how multiple widgets can communicate between each other using singals and slots
'''

def main():
  app = QApplication(sys.argv)
  app.setStyle('Cleanlooks')
  tw = ThreadedWidgetCollection()
  sys.exit(app.exec_())

if __name__ == '__main__':
  main()
